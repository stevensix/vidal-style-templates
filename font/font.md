# Styleguide for the Vidal Corporate identity


## typography

We use 'Ruda' as fonttype to display the Vidal identity

```
https://www.google.com/fonts/specimen/Ruda

```

### alternative

http://processtypefoundry.com/fonts/klavika/complete-specs
